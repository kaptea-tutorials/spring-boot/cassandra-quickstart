package com.cassandra.cassandrastarter;


import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CassandraRepository<Employee, String> {

}
