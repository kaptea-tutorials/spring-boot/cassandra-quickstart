package com.cassandra.cassandrastarter;

import java.util.List;

public interface EmployeeService {

    Employee createNewEmployee(Employee employee);

    Iterable<Employee> getEmployees();
}
