package com.cassandra.cassandrastarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassandraStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(CassandraStarterApplication.class, args);
	}

}

